#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import json
from datetime import datetime

response = json.loads(requests.get('https://fairo.exchange/fair.json').text)
if( response ):
    response ['FAIRO']['timestamp']=datetime.now().timestamp()
    open( './services/jekyll/_data/price-sources/FAIRO.json','w').write( json.dumps( response['FAIRO'] ) )
