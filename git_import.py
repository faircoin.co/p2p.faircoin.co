#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import hashlib
import sys
import yaml
import shutil
from datetime import datetime

QPATH='./services/jekyll/_site/queue'
IPATH='./services/jekyll/images'
TPATH='./services/jekyll/_data/orderbook'
RPATH='./services/tmp'


def checkUserYml():

    if( not os.path.exists(RPATH+'/user.yml') ):
        print( uname + ' - user.yml not found in repository' )
        return

    with open( RPATH + '/user.yml', 'r') as stream:
        try:
            u=yaml.safe_load(stream)
            if( 'country' in u and 'lang' in u and 'latitude' in u and 'longitude' in u and 'faircoinaddress' in u ):
                print('user.yml validated successfully')
                os.system( 'rm -rf ' + TPATH + '/' + uname )
                os.mkdir( TPATH + '/' + uname )
                shutil.move( RPATH + '/user.yml', TPATH + '/' + uname + '/' )
                getRepoYml()
            else:
                print( 'field list does not match ( county, lang, latitude, longitude, faircoinaddress )' )

        except yaml.YAMLError as exc:
            print('user.yml invalid')

def getRepoYml():
    for y in os.listdir( RPATH ):
        if( not os.path.isfile( RPATH + '/' + y ) ):
            continue
        if( y == 'user.yml'):
            continue
        if( y[-4:] != '.yml' ):
            continue
        with open( RPATH + '/' + y, 'r') as stream:
            try:
                u=yaml.safe_load(stream)
                if( u == None ):
                    continue

                if( not 'bid-ask' in u ):
                    print( 'field "bid-ask" not found but mandatory, ' + y + ' skipped!' )
                    continue

                if( not 'condition' in u ):
                    u['condition']='2-used-mint'

                if( not 'min' in u ):
                    u['min']=0

                if( not 'max' in u ):
                    print( 'field "max" not found but mandatory, ' + y + ' skipped!' )
                    continue

                if( not 'unit' in u ):
                    u['unit']='x'

                if( not 'price-per-unit' in u ):
                    print( 'field "price-per-unit" not found but mandatory, ' + y + ' skipped!' )
                    continue

                if( not 'price-reference' in u ):
                    print( 'field "price-reference" not found but mandatory, ' + y + ' skipped!' )
                    continue

                if( not 'headline' in u ):
                    print( 'field "headline" not found but mandatory, ' + y + ' skipped!' )
                    continue

                if( not 'tags' in u ):
                    print( 'field "tags" not found but mandatory, ' + y + ' skipped!' )
                    continue

                if( not 'image' in u ):
                    u['image']=0
                else:
                    if( u['image'] ):
                        if( not os.path.isdir( IPATH ) ):
                            os.mkdir( IPATH )
                        if( os.path.isfile( RPATH + '/images/' + y[:-4] + '.jpg' ) ):
                            if( not os.path.isdir( IPATH + '/' + uname ) ):
                                os.mkdir( IPATH + '/' + uname )
                            shutil.move( RPATH + '/images/' + y[:-4] + '.jpg', IPATH + '/' + uname + '/' )

                if( not 'description' in u ):
                    u['description']=''

                u['time'] = datetime.now().timestamp()

                print(y+' validated successfully')
                open( TPATH + '/' + uname + '/' + y, 'w').write( yaml.dump(u, explicit_start=True, default_flow_style=False) )
                # os.system( 'mv '+ RPATH + '/'+y+' ' + TPATH + '/' + uname + '/' )

            except yaml.YAMLError as exc:
                print(y+' invalid')

#if( os.path.exists( PATH ) ):
#	os.rmdir( PATH )

if( os.path.exists(QPATH) ):
    shutil.move( QPATH, './queue' )
    for f in os.listdir( './queue' ):
        if( os.path.isfile( './queue/' + f ) ):
            h=hashlib.md5(open( './queue/' + f,'rb').read()).hexdigest()
            repo=open( './queue/' + f,'r').read()
            os.system( 'rm -rf '+RPATH )
            os.system( 'git clone '+repo+' --config core.sshCommand="ssh -i ~/.ssh/p2p" ' + RPATH )
            uname = repo.split('/')[0].split(':')[1]
            os.system( 'rm -rf ' + IPATH + '/' + uname )
            os.system( 'rm -rf ' + TPATH + '/' + uname )
            checkUserYml()

            os.system( 'rm ./queue/'+f )
