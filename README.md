# P2P-FairExchange

Smart and simple exchange managed by git issue tracking system.

### Hierachy

| Layer |  | specification |
|:---:|:---:| --- |
| 1 | Country | [ISO2](https://www.countrycode.org/) |
| 2 | Category | [Categories](#Categories) |
| 3 | Condition | [Condition](#Condition) |
| 4 | Type | [Type](#Type) |


#### Categories
| labels | additional tags |
| --- | --- |
| eat_and_drink | non-vegetarian<br>vegetarian<br>vegan |
| home_and_living | appliance<br>construction<br>interior<br>supply<br>storage |
| landscaping | furnishing<br>horticulture |
| electronics | home-electronics<br>multimedia<br>communication<br>illumination |
| tools-and-materials | tools<br>materials<br>storage |
| clothing_and_lifestyle | underwear<br>workwear<br>clothes<br>accessories |
| bodycare_and_health | bodycare<br>health |
| art_and_culture | artwork<br>cultural-goods |
| services | craft<br>social<br>styling<br>design<br>repair<br>guidance |
| knowledge_and_information | soft-media<br>hard-media |
| learning_and_development | workshops<br>teaching |
| mobility_and_transport | cars<br>bicycles<br>ships<br>accessories |
| accommodation | rooms<br>houses |

#### Condition

| label | description |
| --- | --- |
| new | product is unused |
| used-mint | product is used but as new |
| used-good | product is used and in good condition with less signs of wear |
| used-moderate | product is used and in moderate condition with some signs of wear |
| used | product with viewable signs of wear but still functional |
| defective-repairable | product is defective but seems to be repairable |
| defective-spareparts | product is defective but can be used to get spare parts |


#### Type

| label | description |
| --- | --- |
| Offer | offer goods and services |
| Need | demand goods and services |


#### crontab ( for auto update )
~~~
22 2 * * *      gitlab-runner   cd /home/gitlab-runner/projects/p2p.faircoin.co && python3 ./price_update.py
*/2 * * * *     gitlab-runner   cd /home/gitlab-runner/projects/p2p.faircoin.co && python3 ./git_import.py
~~~
