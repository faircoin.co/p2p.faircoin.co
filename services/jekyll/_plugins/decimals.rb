
module Numbers
  def decimals(input)
    '%.2f' % input
  end
end

Liquid::Template.register_filter(Numbers) # register filter globally
