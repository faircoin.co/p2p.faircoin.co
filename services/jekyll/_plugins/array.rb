
module Arrays

  def clean(input)
    input.delete_if {|value| value == nil}
    input[0]
  end

  def merge(a1,a2)
    obj={}
    for a in a1[0]
      obj[a[0]]=a[1]
    end
    for a in a2
      obj[a[0]]=a[1]
    end
    obj
  end

  def objectsize(input)
    c=0
    for a in input
      for aa in a[1]
        if aa['test'] != true
          c+=1
        end
      end
    end
    c
  end



end

Liquid::Template.register_filter(Arrays) # register filter globally
