require 'fastimage'

module Image
  def imagesize(input)
    FastImage.size(input)
  end

  def src_extract(input)

    e=input.split(/src=\"/)

    if e.length == 2
      e=e[1].split(/\"/)
      return e[0]
    else
      return ''
    end

  end

end

Liquid::Template.register_filter(Image) # register filter globally
