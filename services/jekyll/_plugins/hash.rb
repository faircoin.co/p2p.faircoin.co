require 'digest'

module Digest
  def sha256(input)
    Digest::SHA256.hexdigest input
  end
end

Liquid::Template.register_filter(Digest) # register filter globally
