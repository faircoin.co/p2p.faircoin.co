---
layout: none
---
<?php

$json = $_POST['data'];

if( empty($json) ) exit;

$Reg_entry=[]; $Reg_entry_test=[];
{% for reg_entry in site.data.register %}array_push( $Reg_entry, '{{ reg_entry[0] }}' );{% if reg_entry[1].test %} array_push( $Reg_entry_test,'{{ reg_entry[0] }}' ); {% endif %}
{% endfor %}

var_dump($Reg_entry);

foreach( $json as $key=>$value ){
  if( in_array( $key, $Reg_entry ) ){
    if( in_array( $key, $Reg_entry_test )) $value[0]['test']=true;
    $fp=fopen('../_data/orderbook/'.$key.'.json','w+');
    fwrite($fp, json_encode( $value, JSON_UNESCAPED_SLASHES ) );
    fclose($fp);
    $fp=fopen('../_data/orderbook-update/'.$key.'.json','w+');
    fwrite($fp, json_encode( Array( 'time' => date('Y-m-d H:i') ) ) );
    fclose($fp);
  }
}
?>
