function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1);
  var a =
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

function getByTag( tags, is_active ){

  $('.orderlist-div').toggleClass('hide',false);

  if( tags[2] != undefined ){
    $('.orderlist-div').toggleClass('d-none',true);
    $('.order-' + tags[2]).toggleClass('d-none',false);
  } else if( tags[0] == undefined ){
    $('.orderlist-div').toggleClass('d-none',false);
    var tag='';
    var subtag='';
  } else {

    if( is_active ) return;
    $('.orderlist-div').toggleClass('hide',true);

    setTimeout(function(){
      $('.orderlist-div').toggleClass('d-none',true);

      // manage tags
      $('.li-tags').toggleClass('active', false );
      $('.li-subtags').toggleClass('active', false ).toggleClass('d-none',true);

      var tag_class='';

      tags.forEach(
        function(v,i){
          if( v == null || v == '' ) return;
          tag_class+='.tag-'+v;
          $('.li-tag-'+v).toggleClass('active',true);
          $('.li-subtag-of-'+v).toggleClass('d-none', false);
        }
      );

      $(tag_class).toggleClass('d-none',false);
      var tag=tags[0];
      var subtag=tags[1];

      // manage url params
      var p=['tag','subtag','lat','long'];

      var lat=undefined;
      var long=undefined;

      var param=[];

      p.forEach(
        function(v){
          if( eval(v) != undefined ){
            var pp=eval(v);
          } else {
            var pp=getQueryVariable(v);
          }
          if( pp != undefined ){
            if( pp != '' ){
              param.push(v + '='+pp);
            }
          }
        }
      );
      ChangeUrl( '',  (( param != '' ) ? '?' : '' )+param.join('&') );

      setTimeout(function(){
        $('.orderlist-div.hide').toggleClass('hide',false);
      },100 );

    },500 );
  }
}

function getDistancesTags(){

  var url_string = window.location.href;
  var url = new URL(url_string);
  var lat = url.searchParams.get('lat');
  var long = url.searchParams.get('long');
  var tag = url.searchParams.get('tag');
  var subtag = url.searchParams.get('subtag');
  var order = url.searchParams.get('order');

  getByTag([ tag, subtag, order ], false );

  if( lat == null || long == null ){
    lat=-100;
  }
  var calc_distance=false;
  if( lat >= -90 && lat <= 90 && long >=-180 && long <= 180 ){
    calc_distance=true;
  }

  $.each( $('.distance'), function(i,v){
    var geo=$( $(v).context.previousElementSibling ).text();
    var llat=geo.split(/,/g)[0];
    var llong=geo.split(/,/g)[1];
    var lcountry=geo.split(/,/g)[2];
    $(v).html('<span class="feather-icon icon-map-pin"></span> ' + lcountry + ' ' + (( calc_distance ) ? '( ' + Math.round( getDistanceFromLatLonInKm (lat, long, llat, llong ) )+'km ) ' : '' ) );
  } );
}

function formInput(obj){
  if( $(obj).hasClass('form-decimals') ){
    $(obj).toggleClass('is-invalid', ! $.isNumeric( $(obj).val() ) );
  }
}

function createYml(){
  var yml='';
  $.each( $('.order-form'),
    function(i,v){
      if( ( v.id == 'price-reference' || v.id == 'tags' ) && false){
        yml+='[ ' + $(v).val().replace(/(?:\r\n|\r|\n)/g, '<br>') + ' ]\t';
      }  else {
        yml+=$(v).val().replace(/(?:\r\n|\r|\n)/g, '<br>') + '\t';
      }

    }
  );
  $('#order-yml').val(yml);
}


function createYml2(){
  var yml='';
  $.each( $('.order-form'),
    function(i,v){
      if( v.id == 'pricee-reference' || v.id == 'tags' ){
        yml+=v.id + ': [ ' + $(v).val().replace(/(?:\r\n|\r|\n)/g, '<br>') + ' ]\n';
      }  else {
        yml+=v.id + ': ' + $(v).val().replace(/(?:\r\n|\r|\n)/g, '<br>') + '\n';
      }

    }
  );
  $('#order-yml').val(yml);
}

function importYml(){
  var yml=$('#order-yml').val();
  var error='';
  $('#order-yml').toggleClass('is-invalid',false);
  var lastf='';
  yml.split('\n').forEach(
    function(v,i){
      $($('.order-form')[i]).val( v.split(/:/g).slice(1).join(':').trim().replace(/\n/g,'') );
    }
  );
}

function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
}

function ChangeUrl(page, query ) {
    if( query == '' ){
      query=window.location.pathname;
    }
    if (typeof (history.replaceState) != "undefined") {
        var obj = { Page: page, Url: query };
        history.replaceState(obj, obj.Page, obj.Url);
    } else {
        alert("Browser does not support HTML5.");
    }
}

function GetValues(){
  $.each( $('.order-form'),
    function(i,v){
      if( getQueryVariable(v.id) != undefined ){
        $(v).val( decodeURI( getQueryVariable(v.id) ) );
      }
    }
  );
}

var receipient = '';
var order_id = '';

const rand=()=>Math.random(0).toString(36).substr(2);
const token=(length)=>(rand()+rand()+rand()+rand()).substr(0,length);

function createMessage( pubkey, r, o ){
  receipient=r;
  order_id=o;
  $('.modal-messager #pubkey').val(pubkey);
  $('.modal-messager #token').val(token().slice(0,4));
  $('.background-area').toggleClass('foreground', true);
  $('.modal-messager').toggleClass('foreground', true);
}

function cancelMessage(){
  $('.background-area').toggleClass('foreground', false);
  $('.modal-messager').toggleClass('foreground', false);
}

function createConsoleEncrypt(){
  var tmp='';
  tmp+="encrypt('"+ $('#pubkey').val() +"',b'"+ $('#message').val()+" [order-id: " + order_id + " | auth-token: " + $('#token').val() + "]')"
  $('#console-encrypt').val(tmp);
}

function createIssue(){
  var url=receipient + '/issues/new?issue[title]=' + order_id;
  url+='&issue[description]=```decrypt(\''+ $('#pubkey').val() +'\',' + encodeURIComponent( $('#console-encrypt-result').val() ) + ')```';
  window.open(url, 'message');
  $('#create-issue').toggleClass('d-none', true);
  $('#cancel').text('Close');
}
