---
---
var OB={{ site.data.orderbook | jsonify }};

var CAT={{ site.data.categories | jsonify }};

var PR={{ site.data.price-sources | jsonify }};

var CUR={{ site.data.currencies | jsonify }};

var PRF={{ site.data.peers | jsonify }};

function createQRCode(elem,peer,order){

  var qrcode=$(elem).parent().parent().next().next();

  if( $(qrcode).html().trim() != '' ){
    $($(qrcode).children()[0]).remove();
    return;
  }

  if( OB[peer] == undefined ){
    alert( 'peer not found!' );
    return false;
  }

  if( OB[peer].user == undefined ){
    alert( 'user.yml of peer not found!' );
    return false;
  }

  if( OB[peer].user.faircoinaddress == undefined ){
    alert( 'faircoinaddress of peer not not found in user.yml!' );
    return false;
  }

  if( OB[peer][order]['price-reference'] == 'FAIRO' ){
    price_ref=PR.FAIRO.last.toFixed(2);
  } else {
    price_ref=1;
  }

  var url='faircoin:'+OB[peer].user.faircoinaddress+'?amount='+(OB[peer][order]['price-per-unit']/price_ref).toFixed(8);

  //if( $(qrcode).html().trim() == '' ){

    $(qrcode).html('<div></div>');
    $(qrcode).children()[0] = new QRCode( $(qrcode).children()[0] , {
        text: url,
        colorDark : "#000000",
        colorLight : "#ffffff",
        correctLevel : QRCode.CorrectLevel.M
    });
    console.log( $(qrcode).children()[0] );
  //} else {
  //  window[qrcode].clear(); // clear the code.
  //  window[qrcode].makeCode(url); // make another code.
  //}

  //$('.qrcode').toggleClass('invisble',true);
  //$('#'+qrcode).parent().toggleClass('invisble');

}


$(document).ready(
  function(){
    showOrderbook();
  }
);

function showOrderbook(){
  var tmp='';
  $.each( OB,
    function( peer,data ){
      if( data.user == undefined ) return;
      $.each( data,
        function(i,order){
          if( i == 'user' ) return;
          if( order.tags == undefined ) return;
          if( order['price-reference'] != 'FAIRO' && order['price-reference'] != 'FAIR' ) return;
          tmp+='<div class="orderlist-div tag-'+order.tags.join(' tag-')+' order-'+i+'">';

          tmp+='  <div class="orderlist-date">'
          tmp+='    <div class="image"><small>'+( new Date(order.time * 1000).toJSON().replace(/T/g,' ').slice(0,-8) )+'</small>';
          if( order.image ){
            tmp+='    <img src="../images/'+peer+'/'+i+'.jpg">';
          }
          tmp+='    </div>';
          tmp+='  </div>';

          tmp+='  <div class="orderlist-title">';
          tmp+='    <a href="form.html?lang='+data.user.lang+'&tags='+order.tags.slice(0,2).join(',')+'&bid-ask='+order['bid-ask']+'&headline='+encodeURI(order.headline)+'&condition='+order.condition+'&description='+encodeURI(order.description)+'&unit='+order.unit+'&min-size='+order['min-size']+'&max-size='+order['max-size']+'&price-reference='+order['price-reference']+'&price-per-unit='+order['price-per-unit']+'"><span class="feather-icon icon-copy" title="Duplicate and Edit Order"></span></a> '+i+' -';
          tmp+='    <u>'+order.headline+'</u><br><small> <a href="'+PRF[peer].profile+'" target="repo">@'+peer+'</a> / <span class="user-address" onclick="createQRCode(this,\''+peer+'\',\''+i+'\')">'+data.user.faircoinaddress+'</span></small>';
          tmp+='    <br>';
          tmp+='    <small><bidask>'+(( order['bid-ask'] == 'ask') ? '<span class="feather-icon icon-help-circle"></span> ask' : '<span class="feather-icon icon-alert-circle"></span> bid' )+'</bidask> <lang><span class="feather-icon icon-tag"></span>'+data.user.lang+'</lang> <a href="https://www.latlong.net/c/?lat='+data.user.latitude.toFixed(6)+'&long='+data.user.longitude.toFixed(6)+'" target="latlong" title="latitude:'+data.user.latitude.toFixed(6)+' | longitude: '+data.user.longitude.toFixed(6)+'"><span class="d-none distance-data">'+data.user.latitude+','+data.user.longitude+','+data.user.country+'</span> <span class="distance"></span></a> <used><span class="feather-icon icon-tag"></span>'+order.condition+'</used>';
          tmp+=order.tags.map( (a,i) => '<tags class="li-tags li-tag-'+a+' clickable" title="'+a+'" onclick="getByTag( [\''+order.tags[0]+'\', \''+a+'\'], $(this).hasClass(\'active\') )">'+(( i == 2 ) ? '<br>' : '')+'<span class="feather-icon icon-tag"></span>'+(( i == 0 ) ? CAT[a].title[ln] : (( CAT[order.tags[0]].Tags[a] != undefined ) ? CAT[order.tags[0]].Tags[a][ln] : ' <custom>'+a+'</custom>' ) ) +'</tags>' ).join(' ');
          tmp+='    </small>';
          tmp+='    <div><small>'+order.description+'</small></div>';
          tmp+='  </div>';

          if( order['price-reference'] == 'FAIRO' ){
            price_ref=PR.FAIRO.last.toFixed(2);
          } else {
            price_ref=1;
          }

          tmp+='  <div class="orderlist-price"><font class="faircoin-price">'+(order['price-per-unit'].toFixed(2))+' '+order['price-reference']+'</font><br>'+(( order['min'] > 0 ) ? order['min']+'..' : '')+order['max']+' '+order.unit+'<br><span class="'+( ( order['price-reference'] == 'FAIR' ) ? 'd-none' : '')+'">( '+(order['price-per-unit']/price_ref).toFixed(0)+' FAIR / '+order.unit+' )<br>( '+price_ref+' '+order['price-reference']+' / FAIR )<br><i><a href="'+CUR.FAIR['price-sources'].FAIRO.www+'"></a></i></span><br><br><br>';
          tmp+='  </div>';

          tmp+='  <div class="qrcode">';
          //tmp+='    <div onclick="" id="qrcode_'+peer+'_'+i+'" data-price="'+( Math.round(order['price-per-unit']/price_ref).toFixed(8) )+'" class=""></div>';
          tmp+='  </div>';

          tmp+='</div>';
        }
      );
    }
  );

  $('.orders').html(tmp);

}
