<?php

/**
 *
 */

define('ELECTRUMFAIR_RPC','http://user:password@faircoin:7777');
define('FAIRCOIND_RPC',   'http://user:password@faircoin:8332');

function regex($s,$p){
  return preg_match('/'.$p.'/',$s);
}

class FaircoinAddress
{
  public $param=Array( 'jsonrpc' => '2.0', 'id' => 'curltext' );
  public $address='';
  public $TX=Array();
  public $FA=Array();
  public $online=false;
  public $result=false;

  function __construct($address, $init='history', $amount=false, $tstamp=0 ){
    $this->address=$address;
    $this->online = !empty( $this->getservers() );
    if( $this->online && $init ){
      if( regex( $init, 'history|fairauth' ) ) $this->getaddresshistory();
      if( $init == 'fairauth' ) $this->getFairAuth();
      if( regex( $init, 'checkpayment' ) ){
        $this->getaddresshistory();
        $this->result=$this->payInCheck($tstamp,$amount);
      }
    }
  }

  function encrypt($pubkey, $message ){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'pubkey' => $pubkey, 'message' => $message );
    $result=json_decode( $this->electrumfair(), true );
    return $result;
  }

  function getservers(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    $result=json_decode( $this->electrumfair(), true )['result'];
    return $result;
  }

  function getaddresshistory($info=true){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'address' => $this->address );
    $result=json_decode( $this->electrumfair(), true )['result'];

    $this->TX=Array();
    if( $info ){
      foreach( $result as $r ){
        $this->TX[ $r['tx_hash'] ]['height'] = $r['height'];
        $this->TX[ $r['tx_hash'] ]['blockhash'] = $this->getblockhash($r['height']);
        $blockinfo=$this->getblock( $this->TX[ $r['tx_hash'] ]['blockhash'] );
        $this->TX[ $r['tx_hash'] ]['confirmations'] = $blockinfo['confirmations'];
        $this->TX[ $r['tx_hash'] ]['time'] = $blockinfo['time'];
        $tx=$this->gettransactions( $r['tx_hash'] );
        $this->TX[ $r['tx_hash'] ]['addr'] = $tx;
      }
    } else {
      $this->TX=$result;
    }

  }

  function gettransactions($txhash, $is_input=false ){

    $result=$this->gettransaction($txhash,$is_input);

    $TXS=Array( 'inputs' => [], 'outputs' => [] );

    if( $is_input == false ){
      ## check inputs by outputs of previous tx
      $txin=$result['inputs'];
      foreach( $txin as $t ){
        //$TXS['inputs'][] = $this->gettransaction( $t['prevout_hash'], true )['outputs'][$t['prevout_n']];
        $TXS['inputs'][$t['address']]=$this->gettransaction( $t['prevout_hash'], true )['outputs'][$t['prevout_n']]['value'];
      }
    }

    $txout=$result['outputs'];
    foreach( $txout as $t ){
      ## check outputs of tx
      $TXS['outputs'][$t['address']]=$TXS[$t['address']] + $t['value'];
    }

    return $TXS;

  }

  function gettransaction($txhash, $is_input=false ){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'txid' => $txhash );
    $result=json_decode( $this->electrumfair(), true );
    $result=json_decode( $this->deserialize( $result['result']['hex'] ), true )['result'];
    return $result;
  }

  function deserialize($hex){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'tx' => $hex );
    return $this->electrumfair();
  }

  function getaddressbalance(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'address' => $this->address );
    return json_decode( $this->electrumfair(), true )['result'];
  }

  function getblockcount(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }

  function getblockhash($index){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( $index );
    return json_decode( $this->faircoind(), true )['result'];
  }

  function getblock($blockhash){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( $blockhash );
    return json_decode( $this->faircoind(), true )['result'];
  }

  function getrawmempool(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }

  function verifymessage($message, $signature){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'address' => $this->address, 'message' => $message, 'signature' => $signature );
    return json_decode( $this->electrumfair(), true )['result'];
  }

  function getFairAuth(){
    foreach($this->TX as $i=>$v){
      if( !empty( $v['addr']['outputs'][$this->address] ) ) {
        // requested address in inputs -> incoming tx
        $fair_amount=$v['addr']['outputs'][$this->address];
        $sender_address=array_intersect_key( array_keys( $v['addr']['inputs'] ), array_keys( $v['addr']['outputs'] ) );
        if( !empty($sender_address) ){
          array_push( $this->FA, array( 'height' => $v['height'], 'time' => $v['time'], 'txhash' => $i, 'fair_addr' => $sender_address[0], 'fair_amount' => $fair_amount/100000000 ) );
        }
      }
    }
  }

  function payInCheck( $tstamp, $pay_in_amount, $wait_confirmation=false ){
    foreach ($this->TX as $i=>$v){
      if( $v['time'] > $tstamp || ( $v['height'] == 0 && !$wait_confirmation ) ){
        if( !empty( $v['addr']['outputs'][$this->address] ) ){
          if( $v['addr']['outputs'][$this->address] == $pay_in_amount ){
            $sender_address=array_intersect_key( array_keys( $v['addr']['inputs'] ), array_keys( $v['addr']['outputs'] ) );
            return array( 'txhash' => $i, 'sender' => ( count( $sender_address ) == 1 ) ? $sender_address[0] : false  );
          }
        }
      }
    }
    return false;
  }

  function payOutCheck( $tstamp, $pay_in_amount ){
    return $this->payInCheck( $tstamp, $pay_in_amount );
  }

  function electrumfair(){ return $this->rpccall(ELECTRUMFAIR_RPC); }
  function faircoind(){ return $this->rpccall(FAIRCOIND_RPC); }

  function rpccall($url){

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST,true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $this->param ) );

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;

  }

}
