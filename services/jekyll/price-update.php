---
layout: null
---
<?php

function curl($url, $post, $param){

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url );
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  if( $post == true ){
    curl_setopt($ch, CURLOPT_POST,true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $param );
  }

  $data = curl_exec($ch);
  curl_close($ch);

  return $data;
}

function getDataBy($data,$param){
  $p=array_shift($param);
  $data=$data[$p];
  if( sizeof($param) > 0 ) $data=getDataBy($data,$param);
  return $data;
}

$fn='price-sources.json';
$fp=fopen($fn,'r');
$json=fread($fp,filesize($fn));
fclose($fp);

$J=json_decode($json,true);

foreach($J as $k1=>$l1 ){
  foreach($l1 as $k2=>$l2){
    foreach($l2 as $k3=>$l3){
      $data=json_decode( curl( $l3['www'],false, '' ), true );
      $d=getDataBy($data,$l3['param']);
      eval('$p='.preg_replace('/\$/',$d,$l3['formula']).';');
      $fn='../_data/price-sources/'.$k1.'-'.$k2.'-'.$l3['file'];
      $P=Array('lastupdate' => date('Y-m-d'), 'currency' => $k2, 'price' => $p, 'www' => $l3['www'] );
      $fp=fopen($fn,'w+');
      fwrite($fp,json_encode($P, JSON_UNESCAPED_SLASHES ));
      fclose($fp);
    }
  }
}

?>
