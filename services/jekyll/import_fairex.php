---
layout: none
---
<?php

$json = $_POST['data'];

if( empty($json) ) exit;

foreach( $json as $key=>$value ){
  $fp=fopen('../_data/orderbook_fairex/'.$key.'.json','w+');
  fwrite($fp, json_encode( $value, JSON_UNESCAPED_SLASHES ) );
  fclose($fp);
}

?>
